﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web.Models;

namespace Web.Controllers
{
    public class AssessmentMarksController : ApiController
    {
        private SrsDBEntities db = new SrsDBEntities();

        // GET: api/AssessmentMarks
        public IQueryable<AssessmentMark> GetAssessmentMarks()
        {
            return db.AssessmentMarks;
        }

        // GET: api/AssessmentMarks/5
        [ResponseType(typeof(AssessmentMark))]
        public IHttpActionResult GetAssessmentMark(int id)
        {
            AssessmentMark assessmentMark = db.AssessmentMarks.Find(id);
            if (assessmentMark == null)
            {
                return NotFound();
            }

            return Ok(assessmentMark);
        }

        // PUT: api/AssessmentMarks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAssessmentMark(int id, AssessmentMark assessmentMark)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != assessmentMark.Id)
            {
                return BadRequest();
            }

            db.Entry(assessmentMark).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssessmentMarkExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AssessmentMarks
        [ResponseType(typeof(AssessmentMark))]
        public IHttpActionResult PostAssessmentMark(AssessmentMark assessmentMark)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(db.AssessmentMarks.Any(m=> m.AssessmentId == assessmentMark.AssessmentId && m.StudentId == assessmentMark.StudentId))
            {
                ModelState.AddModelError("AssessmentId", "Student already received mark for the assessment");
                return BadRequest(ModelState);
            }

            db.AssessmentMarks.Add(assessmentMark);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = assessmentMark.Id }, assessmentMark);
        }

        // DELETE: api/AssessmentMarks/5
        [ResponseType(typeof(AssessmentMark))]
        public IHttpActionResult DeleteAssessmentMark(int id)
        {
            AssessmentMark assessmentMark = db.AssessmentMarks.Find(id);
            if (assessmentMark == null)
            {
                return NotFound();
            }

            db.AssessmentMarks.Remove(assessmentMark);
            db.SaveChanges();

            return Ok(assessmentMark);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AssessmentMarkExists(int id)
        {
            return db.AssessmentMarks.Count(e => e.Id == id) > 0;
        }
    }
}