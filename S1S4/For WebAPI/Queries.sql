﻿CREATE TABLE [dbo].[Students]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [WiutID] NVARCHAR(MAX) NOT NULL, 
    [FullName] NVARCHAR(MAX) NOT NULL, 
    [DateOfBirth] DATETIME NOT NULL
)
GO

INSERT INTO [Students] (WiutID, FullName, DateOfBirth)
VALUES ('00001896', 'Mikhail Shpirko', '1993-06-23')

INSERT INTO [Students] (WiutID, FullName, DateOfBirth)
VALUES ('00002063', 'Farrukh Normuradov', '1992-08-17')
GO

CREATE TABLE [dbo].[Modules]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Code] NVARCHAR(MAX) NOT NULL, 
    [Title] NVARCHAR(MAX) NOT NULL, 
    [Level] INT NOT NULL
)
GO

INSERT INTO [Modules](Code, Title, [Level])
VALUES ('5BUIS015C', 'Game Development', 5)

INSERT INTO [Modules](Code, Title, [Level])
VALUES ('6BUIS008C', 'Web Applications Development', 6)
GO


CREATE TABLE [dbo].[Assessments]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ModuleId] INT NOT NULL, 
    [Title] NVARCHAR(MAX) NOT NULL, 
    [Weight] INT NOT NULL, 
    CONSTRAINT [FK_Assessments_Modules] FOREIGN KEY ([ModuleId]) REFERENCES [Modules]([Id])
)
GO

INSERT INTO [Assessments] (ModuleId, Title, [Weight])
VALUES (1, 'Portfolio', 100)

INSERT INTO [Assessments] (ModuleId, Title, [Weight])
VALUES (2, 'CW1', 30)

INSERT INTO [Assessments] (ModuleId, Title, [Weight])
VALUES (2, 'CW2', 70)
GO

CREATE TABLE [dbo].[AssessmentMarks]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [AssessmentId] INT NOT NULL, 
    [StudentId] INT NOT NULL, 
    [Mark] INT NOT NULL, 
    CONSTRAINT [FK_AssessmentMarks_Assessments] FOREIGN KEY ([AssessmentId]) REFERENCES [Assessments]([Id]), 
    CONSTRAINT [FK_AssessmentMarks_Students] FOREIGN KEY ([StudentId]) REFERENCES [Students]([Id])
)
GO

INSERT INTO [AssessmentMarks] (AssessmentId, StudentId, Mark)
VALUES (1, 1, 95)

INSERT INTO [AssessmentMarks] (AssessmentId, StudentId, Mark)
VALUES (1, 2, 80)

INSERT INTO [AssessmentMarks] (AssessmentId, StudentId, Mark)
VALUES (2, 1, 60)

INSERT INTO [AssessmentMarks] (AssessmentId, StudentId, Mark)
VALUES (2, 2, 70)

INSERT INTO [AssessmentMarks] (AssessmentId, StudentId, Mark)
VALUES (3, 1, 80)

INSERT INTO [AssessmentMarks] (AssessmentId, StudentId, Mark)
VALUES (3, 2, 70)
GO