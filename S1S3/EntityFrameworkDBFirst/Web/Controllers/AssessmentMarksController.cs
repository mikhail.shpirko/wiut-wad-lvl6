﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class AssessmentMarksController : Controller
    {
        private SrsDBEntities db = new SrsDBEntities();

        // GET: AssessmentMarks
        public ActionResult Index()
        {
            var assessmentMarks = db.AssessmentMarks.Include(a => a.Assessment).Include(a => a.Student);
            return View(assessmentMarks.ToList());
        }

        // GET: AssessmentMarks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssessmentMark assessmentMark = db.AssessmentMarks.Find(id);
            if (assessmentMark == null)
            {
                return HttpNotFound();
            }
            return View(assessmentMark);
        }

        // GET: AssessmentMarks/Create
        public ActionResult Create()
        {
            ViewBag.AssessmentId = new SelectList(db.Assessments, "Id", "Title");
            ViewBag.StudentId = new SelectList(db.Students, "Id", "WiutID");
            return View();
        }

        // POST: AssessmentMarks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,AssessmentId,StudentId,Mark")] AssessmentMark assessmentMark)
        {
            if (ModelState.IsValid)
            {
                db.AssessmentMarks.Add(assessmentMark);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AssessmentId = new SelectList(db.Assessments, "Id", "Title", assessmentMark.AssessmentId);
            ViewBag.StudentId = new SelectList(db.Students, "Id", "WiutID", assessmentMark.StudentId);
            return View(assessmentMark);
        }

        // GET: AssessmentMarks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssessmentMark assessmentMark = db.AssessmentMarks.Find(id);
            if (assessmentMark == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssessmentId = new SelectList(db.Assessments, "Id", "Title", assessmentMark.AssessmentId);
            ViewBag.StudentId = new SelectList(db.Students, "Id", "WiutID", assessmentMark.StudentId);
            return View(assessmentMark);
        }

        // POST: AssessmentMarks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AssessmentId,StudentId,Mark")] AssessmentMark assessmentMark)
        {
            if (ModelState.IsValid)
            {
                db.Entry(assessmentMark).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AssessmentId = new SelectList(db.Assessments, "Id", "Title", assessmentMark.AssessmentId);
            ViewBag.StudentId = new SelectList(db.Students, "Id", "WiutID", assessmentMark.StudentId);
            return View(assessmentMark);
        }

        // GET: AssessmentMarks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssessmentMark assessmentMark = db.AssessmentMarks.Find(id);
            if (assessmentMark == null)
            {
                return HttpNotFound();
            }
            return View(assessmentMark);
        }

        // POST: AssessmentMarks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AssessmentMark assessmentMark = db.AssessmentMarks.Find(id);
            db.AssessmentMarks.Remove(assessmentMark);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
