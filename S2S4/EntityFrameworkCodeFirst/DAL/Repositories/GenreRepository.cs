﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class GenreRepository : IRepository<Genre>
    {
        private readonly StoreContext _context = new StoreContext();

        public IQueryable<Genre> GetAll()
        {
            return _context.Genres;
        }

        public Genre GetById(int id)
        {
            return _context.Genres.SingleOrDefault(e=> e.Id ==id);
        }

        public void Create(Genre entity)
        {
            _context.Genres.Add(entity);
            _context.SaveChanges();
        }


        public void Update(Genre entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            Genre genre = GetById(id);
            _context.Genres.Remove(genre);
            _context.SaveChanges();
        }      

    }
}
