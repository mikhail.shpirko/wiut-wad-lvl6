﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class BookRepository : IRepository<Book>
    {
        private readonly StoreContext _context = new StoreContext();

        public IQueryable<Book> GetAll()
        {
            return _context.Books;
        }

        public Book GetById(int id)
        {
            return _context.Books.SingleOrDefault(e => e.Id == id);
        }

        public void Create(Book entity)
        {
            _context.Books.Add(entity);
            _context.SaveChanges();
        }


        public void Update(Book entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            Book book = GetById(id);
            _context.Books.Remove(book);
            _context.SaveChanges();
        }

    }
}
