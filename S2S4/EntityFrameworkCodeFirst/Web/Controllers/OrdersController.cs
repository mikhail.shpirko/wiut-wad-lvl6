﻿using DAL.Entities;
using DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace Web.Controllers
{
    public class OrdersController : Controller
    {
        private BookRepository _bookRepo = new BookRepository();
        private OrderRepository _orderRepo = new OrderRepository();

        // GET: Orders
        public ActionResult Index()
        {
            var orders = _orderRepo.GetAll();
            return View(orders.ToList());
        }

        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = _orderRepo.GetById(id.Value);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            ViewBag.BookId = new SelectList(_bookRepo.GetAll(), "Id", "Title");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,BookId,Quantity,DeliveryAddress")] Order order)
        {
            if (ModelState.IsValid)
            {
                _orderRepo.Create(order);
                return RedirectToAction("Index");
            }

            ViewBag.BookId = new SelectList(_bookRepo.GetAll(), "Id", "Title", order.BookId);
            return View(order);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = _orderRepo.GetById(id.Value);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.BookId = new SelectList(_bookRepo.GetAll(), "Id", "Title", order.BookId);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,BookId,Quantity,DeliveryAddress")] Order order)
        {
            if (ModelState.IsValid)
            {
                _orderRepo.Update(order);
                return RedirectToAction("Index");
            }
            ViewBag.BookId = new SelectList(_bookRepo.GetAll(), "Id", "Title", order.BookId);
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = _orderRepo.GetById(id.Value);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _orderRepo.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
