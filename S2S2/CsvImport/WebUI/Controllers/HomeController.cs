﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View(new ImportViewModel());
        }

        [HttpPost]
        public ActionResult Index(ImportViewModel model)
        {
            if(model.ImportFile != null)
            {
                using (var reader = new StreamReader(model.ImportFile.InputStream))
                {
                    using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                    {
                        csv.Configuration.HasHeaderRecord = model.IgnoreHeaderLine;
                        var products = csv.GetRecords<ProductImportModel>();
                        model.Products = products.ToList();
                    }
                }
            }

            return View(model);
        }
    }
}