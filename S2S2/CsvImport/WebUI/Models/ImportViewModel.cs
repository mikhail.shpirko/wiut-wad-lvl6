﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    public class ImportViewModel
    {
        public bool IgnoreHeaderLine { get; set; }
        public HttpPostedFileBase ImportFile { get; set; }

        public List<ProductImportModel> Products { get; set; } = new List<ProductImportModel>();
    }
}