﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Registration()
        {
            return View(new RegistrationVM());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Registration(RegistrationVM model)
        {
            if(model.Password != model.ConfirmPassword)
                ModelState.AddModelError("ConfirmPassword", "Does not match with the password");

            if (ModelState.IsValid)
            {
                if(db.Users.Any(u=> u.Email == model.Email))
                {
                    ModelState.AddModelError("Email", "User with such email already registered");
                }
                else
                {
                    var user = new User
                    {
                        Email = model.Email,
                        Password = model.Password,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Address = model.Address
                    };

                    db.Users.Add(user);
                    db.SaveChanges();

                    
                    return RedirectToAction("Login");
                }

            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View(new LoginVM());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginVM model)
        {
            if(ModelState.IsValid)
            {
                if (!db.Users.Any(u => u.Email == model.Email && u.Password == model.Password))
                {
                    ModelState.AddModelError("Email", "Wrong credentials");
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(model.Email, false);
                    return RedirectToAction("Index");
                }
                
            }

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}