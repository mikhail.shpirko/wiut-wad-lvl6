﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebUI.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("name = MyContextDB") { }
        public virtual DbSet<User> Users { get; set; }
    }
}