﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        private static NLog.Logger logger = NLog.LogManager.GetLogger("WebSite");
        // GET: Home
        public ActionResult Index()
        {
            logger.Info("Somebody just entered Index page of Home controller");

            try
            {
                throw new Exception("This exception is to test logging");
            }
            catch (Exception e)
            {

                logger.Error($"Error occured: {e}");
            }

            return View();
        }
    }
}