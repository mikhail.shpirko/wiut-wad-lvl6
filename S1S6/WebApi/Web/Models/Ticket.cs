﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Web.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public int FlightId { get; set; }
        public string Row { get; set; }
        public int Sit { get; set; }
        public TicketClass Class { get; set; }
        public int BaggageWeight { get; set; }
        public int LuggageWeight { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Flight Flight { get; set; }
    }

    public enum TicketClass
    {
        FirstClass,
        BusinessClass,
        EconomyClass
    }
}