﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public int GenreId { get; set; }
        public DateTime IssueDate { get; set; }
        public int NumberOfPrintedCopies { get; set; }

        public virtual Genre Genre { get; set; }
    }
}