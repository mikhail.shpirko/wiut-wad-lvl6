﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Models
{
    public class Book
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Author { get; set; }
        public int Genre { get; set; }
        [DisplayName("Date of Issue")]
        public DateTime IssueDate { get; set; }
        [Range(0, int.MaxValue)]
        [DisplayName("Number Of Printed Copies")]
        public int NumberOfPrintedCopies { get; set; }

        public List<SelectListItem> GenreList => new List<SelectListItem> {
            new SelectListItem { Value = "1", Text = "Horror" },
            new SelectListItem { Value = "2", Text = "Classic" },
            new SelectListItem { Value = "3", Text = "Fiction" },
            new SelectListItem { Value = "4", Text = "Fantasy" },
            new SelectListItem { Value = "5", Text = "Other" }
        };
    }
}