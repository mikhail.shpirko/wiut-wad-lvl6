﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Registration()
        {
            return View(new RegistrationVM());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Registration(RegistrationVM model)
        {
            if(model.Password != model.ConfirmPassword)
                ModelState.AddModelError("ConfirmPassword", "Does not match with the password");

            if (ModelState.IsValid)
            {
                if(db.Users.Any(u=> u.Email == model.Email))
                {
                    ModelState.AddModelError("Email", "User with such email already registered");
                }
                else
                {
                    var user = new User
                    {
                        Email = model.Email,
                        Password = model.Password,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Address = model.Address
                    };

                    db.Users.Add(user);
                    db.SaveChanges();

                    
                    return RedirectToAction("Login");
                }

            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View(new LoginVM());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginVM model)
        {
            if(ModelState.IsValid && ValidateCaptcha())
            {                
                if (!db.Users.Any(u => u.Email == model.Email && u.Password == model.Password))
                {
                    ModelState.AddModelError("Email", "Wrong credentials");
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(model.Email, false);
                    return RedirectToAction("Index");
                }
                
            }

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        private class CaptchaResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("error-codes")]
            public List<string> ErrorCodes { get; set; }
        }

        private bool ValidateCaptcha()
        {
            var client = new WebClient();
            var reply =
                client.DownloadString(
                    string.Format("https://www.google.com/recaptcha/api/siteverify?secret=6LdGOsQUAAAAAKcVwY5qeZVPrLd7dk1126EQzBCo&response={0}", Request["g-recaptcha-response"]));
            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);
            //when response is false check for the error message
            if (!captchaResponse.Success)
            {
                if (captchaResponse.ErrorCodes.Count <= 0) return false;
                var error = captchaResponse.ErrorCodes[0].ToLower();
                switch (error)
                {
                    case ("missing-input-secret"):
                        ModelState.AddModelError("", "The secret parameter is missing.");
                        break;
                    case ("invalid-input-secret"):
                        ModelState.AddModelError("", "The secret parameter is invalid or malformed.");
                        break;
                    case ("missing-input-response"):
                        ModelState.AddModelError("", "The response parameter is missing. Please, preceed with reCAPTCHA.");
                        break;
                    case ("invalid-input-response"):
                        ModelState.AddModelError("", "The response parameter is invalid or malformed.");
                        break;
                    default:
                        ModelState.AddModelError("", "Error occured. Please try again");
                        break;
                }
                return false;
            }

            return true;
        }
    }
}