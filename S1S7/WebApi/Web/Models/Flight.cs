﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Web.Models
{
    public class Flight
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Departure { get; set; }
        public string Destination { get; set; }
        public string Airlines { get; set; }

        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}